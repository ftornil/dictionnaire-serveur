const JDM_URL = 'http://www.jeuxdemots.org/rezo-dump.php?gotermsubmit=Chercher&gotermrel=%s&rel=%s'
//https://cors-anywhere.herokuapp.com/
const JDM_URL_AUTOCOMPLETE = 'http://www.jeuxdemots.org/autocompletion/autocompletion.php?completionarg=gotermrel'
const express = require('express')
var app = express()
var cors = require('cors')
var XMLHttpRequest = require('xhr2');
var events = require('events');
var eventEmitter = new events.EventEmitter();
const path = require('path')
const PORT = process.env.PORT || 5001
const fs = require('fs');
const USE_CACHE = true;

app.use(cors())
app.use(express.static(path.join(__dirname, '.')))
app.set('views', path.join(__dirname, 'html'))
app.set('view engine', 'html')

// OK
app.get('/', (req, res) => res.render('index'))

// OK
app.get('/historique', (req, res) => {
    res.write(fs.readFileSync('mots.txt', 'utf8'))
    res.end()
})

// OK
app.get('/relation', (req, res) => {
    res.write(fs.readFileSync('relations_name.txt', 'utf8'))
    res.end()
})

// OK
app.get('/autocomplete', (req, res) => {
    let mot = req.query['mot']
    if(mot === undefined || mot === "") { res.end() }
    else {
        let event_name = "AutocompleteRequestEnded"
        eventEmitter.once(event_name, (result) => {
            res.write(result)    
            res.end()
        })
        requestAutocomplete(mot, event_name)
    }
})

function requestAutocomplete(mot_a_completer, event_name) {
    let url = JDM_URL_AUTOCOMPLETE
    let request = new XMLHttpRequest();
    request.open('POST', url, true)
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8")

    data = "gotermrel="+mot_a_completer
    request.onload = function() {
        eventEmitter.emit(event_name, request.responseText)   
    }

    request.send(data)
}

// NOK
app.get(/^\/(.+)(\/)?/, (req, res) => {
    let mot = req.params[0]
    let relation = req.query['rela']
    let tri_alpha = req.query['tri'] === 'alpha'
    let query_relation = req.query['type'] === 'r'

    let event_name = "JDM_word_request"+mot+relation
    eventEmitter.once(event_name, (result) => {
        res.write(result)
        res.end()
    })
    
    requestJDM(mot, relation, tri_alpha, query_relation, event_name)
    appendWord(mot, "mots.txt")
})

app.listen(PORT, () => console.log('Serveur running on port ' + PORT))

// Requête jdm ou le cache 
function requestJDM(terme, relation, alpha, query_relation, event_name_out) {
    if (fs.existsSync("cache/"+terme+"_def.txt") 
        && fs.existsSync("cache/"+terme+"_"+relation+'_entites.txt')
        && fs.existsSync("cache/"+terme+"_"+relation+'_rela_type.txt')
        && fs.existsSync("cache/"+terme+"_"+relation+'_rela_out.txt')
        && fs.existsSync("cache/"+terme+"_"+relation+'_rela_in.txt')
        && fs.existsSync("cache/"+terme+"_"+relation+'_in_tri_poids.txt')
        && fs.existsSync("cache/"+terme+"_"+relation+'_in_tri_alpha.txt')
        && fs.existsSync("cache/"+terme+"_"+relation+'_out_tri_poids.txt')
        && fs.existsSync("cache/"+terme+"_"+relation+'_out_tri_alpha.txt')
        && USE_CACHE) {

        let formatedText = ""
        if(relation == -1){
            formatedText += "<mot>"+terme+"</mot>"
            let def = fs.readFileSync("cache/"+terme+'_def.txt', 'utf8');
            formatedText += "<definition>"+def+"</definition>"
        }
        let entites = ""
        if(query_relation){
            if (alpha){
                entites = fs.readFileSync("cache/"+terme+'_'+relation+'_in_tri_alpha.txt', 'utf8')
            } else {
                entites = fs.readFileSync("cache/"+terme+'_'+relation+'_in_tri_poids.txt', 'utf8')
            }
        } else {
            if (alpha){
                entites = fs.readFileSync("cache/"+terme+'_'+relation+'_out_tri_alpha.txt', 'utf8')
            }else{
                entites = fs.readFileSync("cache/"+terme+'_'+relation+'_out_tri_poids.txt', 'utf8')
            }
        }
        formatedText += "<"+relation+"_entites>"+entites+"</"+relation+"_entites>"
        let relation_text = fs.readFileSync("cache/"+terme+"_"+relation+'_rela_type.txt', 'utf8');
        let relation_type_text = clearRelationText(relation_text)
        formatedText += "<relation_type>"+relation_type_text+"</relation_type>"

        eventEmitter.emit(event_name_out, formatedText)

    } else if(fs.existsSync("cache/"+terme+"_erreur.txt")) {
        let erreur_text = fs.readFileSync("cache/"+terme+'_erreur.txt', 'utf8')
        eventEmitter.emit(event_name_out, erreur_text)
    } else {
        let url = JDM_URL
        if(relation == -1) {
            url = url.replace('%s', terme).replace('%s','-1') + "&relout=norelout&relin=norelin"
        } else {
            url = url.replace('%s', terme).replace('%s', relation)
        }
        let request = new XMLHttpRequest();
        request.open('GET', url, true)
        request.overrideMimeType('text/html; charset=iso-8859-1')
    
        request.onload = function() {
            let event_name = "JDMReponseTraitee"+terme+relation
            eventEmitter.once(event_name, () => {
                requestJDM(terme, relation, alpha, query_relation, event_name_out)
            })
            traiterReponseJDM(request.responseText, terme, relation, event_name)
            
        }
        request.send()
    }
}

// Parse la réponse de JDM
function traiterReponseJDM(response_text, terme, relation, event_name_out) {
    let indice_debut = response_text.search("<CODE>") 
    if (indice_debut == -1){
        let erreur_text =  "<erreur>Erreur : "+ terme+ " " +relation+"</erreur>"
        let event_name_in = "NotFoundWordSuggestion"+terme+relation
        eventEmitter.once(event_name_in, (result) => {
            erreur_text += "<suggestions>"+result+"</suggestions>"
            fs.writeFileSync("cache/"+terme+"_erreur.txt", erreur_text)
            eventEmitter.emit(event_name_out)
        })
        requestAutocomplete(terme, event_name_in)
    }
    else {
        indice_debut += 6
        let texte = response_text.substring(indice_debut, response_text.length)

        // Definition
        texte = texte.substring(
            texte.search('<def>') + 5,
            texte.length)
        let fin_definition = texte.search('</def>\n')
        let definition = removeBalises(texte.substring(0, fin_definition))

        // Entités
        texte = texte.substring(
            texte.search('\ne;') + 1,
            texte.length)
        let fin_des_entites = texte.search('\n\n//')
        let entites_texte = texte.substring(0, fin_des_entites)
        texte = texte.substring(fin_des_entites)

        // Relation Type
        texte = texte.substring(
            texte.search('\nrt;') + 1,
            texte.length)
        let fin_des_relation_type = texte.search('\n\n//')
        let relation_type_texte = texte.substring(0, fin_des_relation_type)
        texte = texte.substring(fin_des_relation_type)
        
        // Relations sortantes
        let debut_relations_in_string = "type;w"
        let debut_relations_in = texte.search(debut_relations_in_string) + debut_relations_in_string.length
        texte = texte.substring(
            debut_relations_in + 2,
            texte.length)
        let fin_des_relations_out = texte.search('\n\n//')
        let relations_out_texte = texte.substring(0, fin_des_relations_out)
        texte = texte.substring(fin_des_relations_out)

        // Relations entrantes
        let debut_relations_out_string = "type;w"
        let debut_relations_out = texte.search(debut_relations_out_string) + debut_relations_out_string.length
        texte = texte.substring(
            debut_relations_out + 2,
            texte.length)
        let fin_des_relations_in = texte.search('\n\n//')
        let relations_in_texte = texte.substring(0, fin_des_relations_in) 

        // Ecrire dans un fichier - def
        fs.writeFileSync("cache/"+terme+"_def.txt", definition)

        // Ecrire dans un fichier - entites
        fs.writeFileSync("cache/"+terme+"_"+relation+"_entites.txt", entites_texte)

        // Ecrire dans un fichier - relation type
        fs.writeFileSync("cache/"+terme+"_"+relation+"_rela_type.txt", relation_type_texte)

        // Ecrire dans un fichier - relation out
        fs.writeFileSync("cache/"+terme+"_"+relation+"_rela_out.txt", relations_out_texte)

        // Ecrire dans un fichier - relation in
        fs.writeFileSync("cache/"+terme+"_"+relation+"_rela_in.txt", relations_in_texte)

        // Ecrire dans un fichier out - tri_poids
        entites_tri_poids = tri(entites_texte, relations_out_texte, false, false)
        fs.writeFileSync("cache/"+terme+"_"+relation+"_out_tri_poids.txt", entites_tri_poids)

        // Ecrire dans un fichier out - tri_alpha
        entites_tri_alph = tri(entites_texte, relations_out_texte, true, false)
        fs.writeFileSync("cache/"+terme+"_"+relation+"_out_tri_alpha.txt", entites_tri_alph)

        // Ecrire dans un fichier in - tri_poids
        entites_tri_poids = tri(entites_texte, relations_in_texte, false, true)
        fs.writeFileSync("cache/"+terme+"_"+relation+"_in_tri_poids.txt", entites_tri_poids)

        // Ecrire dans un fichier out - tri_alpha
        entites_tri_alph = tri(entites_texte, relations_in_texte, true, true)
        fs.writeFileSync("cache/"+terme+"_"+relation+"_in_tri_alpha.txt", entites_tri_alph)

        eventEmitter.emit(event_name_out)
    }
}

// Supprimer les _
function clearText(response_text){
    let tab = response_text.replace(/'/g, '').split('\n')
    tab.shift()
    if (tab != undefined){
        tab.forEach(row => {
            rowtab = row.split(';')
            if (rowtab[2].startsWith('_')) {
                tab.splice(tab.indexOf(row),1)
            }
        })
    }
    return tab
}

// Nettoie le texte, plus spécifique le type de relation (pas formaté comme le reste)
function clearRelationText(response_text){
    let cleared_text = []
    let tab = response_text.split(';')
    if (tab != undefined){
        if(tab[3]){
            // appendWord(tab[1]+";"+tab[3], "relations_name.txt")
            cleared_text = tab[1] + " - " + tab[3].toString()
            if (tab[4]) {
                // Au survol - à mettre dans deux balise différentes
                cleared_text += " : " + tab[4].toString()
            }
        }
    }
    return cleared_text.toString()
}

// Enlève les balises
function removeBalises(text){
    let regexp = /<(.*)>/;
    let blank = "^\\s+$";
    while (text.search("<") != -1){
        text = text.replace(regexp, "")
    }
    while (text.search("\n\n") != -1){
        text = text.replace("\n\n", "\n")
    }

    if (text.startsWith(blank) || text.startsWith("\n") || text.startsWith("\t")){
        text = text.substring(1)
    }

    return text
}

// Ajout un mot à un fichier donné
function appendWord(word, file){
    if (fs.existsSync(file)){
        let words = fs.readFileSync(file, 'utf8');
        words_tab = words.split('\n')
        if (words_tab.indexOf(word) == -1){
            fs.appendFile(file, word+"\n", function (err) {
                if (err) throw err;
            });
        }
    }
}

// True = ordre alphabétique, false = poids
function tri(entites, relations, alpha, rela_in){
    // Tri relation
    // Garder uniquement poids >10 par exemple
    let entites_tab = clearText(entites)
    let relation_tab = relations.split('\n')
    let res_tab = []
    let indice_tab_r = 3
    if (rela_in){
        indice_tab_r = 2
    }

    if (!alpha){
        relation_tab.sort(function (a, b) {
            if (Math.abs(a.split(';')[5]) > Math.abs(b.split(';')[5])) {
                return -1
            }else{
                return 1
            }
        });
    }



    relation_tab.forEach(r => {
        r_splitted = r.split(';')
        let rela_neg = false
        if (r_splitted[5] < 0){
            rela_neg = true
        }
        entites_tab.forEach(e => {
            e_splitted = e.split(';')
            if (r_splitted[indice_tab_r] == e_splitted[1]){
                if (e_splitted[5] != undefined && !e_splitted[5].startsWith('_')){
                    if (rela_neg){
                        res_tab.push("<font color='red'>"+e_splitted[5]+"</font>")
                    }else {
                        res_tab.push(e_splitted[5])
                    }
                }else{
                    if (!e_splitted[2].startsWith('_')){
                        if (rela_neg){
                            res_tab.push("<font color='red'>"+e_splitted[2]+"</font>")
                        }else{
                            res_tab.push(e_splitted[2])
                        }
                    }
                }
                entites_tab.splice(entites_tab.indexOf(e),1)
            }
        })
    })


    // sourics car je mets balise rouges
    if (alpha){
        return res_tab.sort(function (a, b) {
            if (a.startsWith("<") && b.startsWith ("<")){
                if (a.substring(18,a.search('</font>')).sansAccent().toLowerCase() > b.substring(18,b.search('</font>')).sansAccent().toLowerCase()) {
                    return 1
                }else{
                    return -1
                }
            } else if (a.startsWith("<")){
                if (a.substring(18,a.search('</font>')).sansAccent().toLowerCase() > b.sansAccent().toLowerCase()) {
                    return 1
                }else{
                    return -1
                }
            } else if (b.startsWith("<")){
                if (a.sansAccent().toLowerCase() > b.substring(18,b.search('</font>')).sansAccent().toLowerCase()) {
                    return 1
                }else{
                    return -1
                }
            } else {
                if (a.sansAccent().toLowerCase() > b.sansAccent().toLowerCase()) {
                    return 1
                }else{
                    return -1
                } 
            }
        });
    }else{
        return res_tab
    }

}

String.prototype.sansAccent = function(){
    var accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g, // C, c
    ];
    var noaccent = ['A','a','E','e','I','i','O','o','U','u','N','n','C','c'];
     
    var str = this;
    for(var i = 0; i < accent.length; i++){
        str = str.replace(accent[i], noaccent[i]);
    }
     
    return str;
}