1. Qui est sensible à la pitié, secourable, bienfaisant. 
 Avoir, montrer des sentiments humains.    
 Je ne connais pas de coeur plus humain.    
 Cet homme est très humain et fort sensible aux misères d'autrui.  
2. Être vivant qui fait partie de l'espèce humaine. 
 J'étais un animal traqué, je ne comprenais plus rien aux humains.    
 (Marie Cardinal, Les mots pour le dire, Livre de Poche, p. 97)  
3. (Au pluriel) L'humanité dans son ensemble.
4. Relatif à l'espèce humaine. 
 Confucius était un moraliste qui se méfiait de l'intelligence ; le terre à terre des relations humaines lui plaisait mieux que la spéculation ondoyante.    
 (Paul Demiéville, La montagne dans l'art littéraire chinois, dans Choix d'études sinologiques (1921-1970), p.364, BRILL, 1973)   
 Le rationalisme cartésien et l'ère nouvelle de la science du XVIIe siècle ont marqué la fin du Moyen Âge et du monopole de l'Église sur les entreprises humaines.    
 (Panayiotis Jerasimof Vatikiotis, L'Islam et l'État, 1987, traduction d'Odette Guitard, 1992, p. 121)   
 Elle se passa les doigts sur les paupières, les yeux reprirent une expression humaine comme si elle revenait d'une pâmoison.    
 (Jean Rogissart, Passantes d'Octobre, Librairie Arthème Fayard, Paris, 1958)   
 La justice, en tant qu'elle veut l'inviolabilité de la personne humaine par le seul fait qu'elle est humaine, ne saurait considérer l'homme que dans l'abstrait.    
 (Julien Benda, La trahison des clercs : Appendice des valeurs cléricales, 1927, éd. 1946)   
 Lorsque l'on ne peut faire autrement que d'affronter le mauvais temps, les forces humaines se décuplent et l'esprit devient plus clairvoyant.    
 (Dieudonné Costes & Maurice Bellonte, Paris-New-York, 1930)   
 Aucune forme humaine ne surgit entre les halliers, aucun bruit humain ne m'arrive. Partout le silence et partout la solitude !    
 (Octave Mirbeau, Le Tripot aux champs, Le Journal, 27 septembre 1896)  
